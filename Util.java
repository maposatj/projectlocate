package msandbox.za.co.projectlocate;

import org.json.JSONException;
import org.json.JSONObject;


public class Util {
	
	public static int getIfNotNull(String key, JSONObject json, int def) {
		try {
			return json.getInt(key);
		} catch (JSONException e) {
			return def;
		}
	}
}
