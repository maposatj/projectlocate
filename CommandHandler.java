package msandbox.za.co.projectlocate;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.Socket;

public class CommandHandler {
	
	private static Context context = null;
	private static Socket socket = null;
	private static Handler handler = null;
	
	public static void set(Context context, Handler handler, Socket socket) {
		CommandHandler.context = context;
		CommandHandler.handler = handler;
		CommandHandler.socket = socket;
	}
	
	public static void sendGps(JSONObject data) {
		
		final LocationManager mlocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		
		final LocationListener locationListener = new LocationListener() {
			
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
			}
			
			@Override
			public void onProviderEnabled(String provider) {
			}
			
			@Override
			public void onProviderDisabled(String provider) {
				JSONObject json = new JSONObject();
		        try {
					json.put("responseMessage", "GPS is disabled");
				} catch (JSONException e) {
				}
		        try {
		        	socket.emit("cMessage", json.toString());
				} catch (Exception e) {
					showToast("H4: " + e.getMessage());
				}
		        mlocManager.removeUpdates(this);
			}
			
			@Override
			public void onLocationChanged(Location location) {
				JSONObject json = new JSONObject();
		        try {
					json.put("latitude", location.getLatitude());
					json.put("longitude", location.getLongitude());
				} catch (JSONException e) {
				}
		        try {
		        	socket.emit("cMessage", json.toString());
				} catch (Exception e) {
					showToast("H4: " + e.getMessage());
				}
		        mlocManager.removeUpdates(this);
			}
		};

		mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, locationListener, Looper.getMainLooper());
	}
	
	public static void toggleGps(boolean enable) {
		
		// If enabled, and wanting to enable, ignore or if disabled, and wanting to disabled
//	    if((isStringContainsGps && enable)
//	    		|| (!isStringContainsGps && !enable)) {
//	        return; // the GPS is already in the requested state
//	    }
		
		Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
	    intent.putExtra("enabled", enable);
	    context.sendBroadcast(intent);

	    String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	    boolean isGpsEnabled = provider.contains("gps");
	    
	    final Intent poke = new Intent();
        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
        poke.setData(Uri.parse("3")); 
        context.sendBroadcast(poke);
        
        JSONObject json = new JSONObject();
        try {
			json.put("responseMessage", "Tried to "+(enable ? "enable" : "disable")+" gps");
		} catch (JSONException e) {
		}
        try {
        	socket.emit("cMessage", json.toString());
		} catch (Exception e) {
			showToast("H5: " + e.getMessage());
		}
	}
	
	protected static void commandDetector(int command, JSONObject data) {
    	switch (command) {
		case Command.GET_GPS:
			CommandHandler.sendGps(data);
			break;
		case Command.ENABLE_GPS:
//			toggleGps(true);
			turnGpsOn();
			break;
		case Command.DISABLE_GPS:
//			toggleGps(false);
			turnGpsOff();
			break;
		default:
			break;
		}
	}
	static String beforeEnable;
	private static void turnGpsOn () {
		if(!isGpsEnabled()) {
		    beforeEnable = Settings.Secure.getString (context.getContentResolver(),
		                                              Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		    String newSet = String.format ("%s,%s", beforeEnable, LocationManager.GPS_PROVIDER);
		    try {
		        Settings.Secure.putString (context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, newSet); 
		    } catch(Exception e) {
		    	showToast(e.getMessage());
		    }
		}
	}


	private static void turnGpsOff () {
	    if(isGpsEnabled()) {
	    	if (null == beforeEnable) {
		        String str = Settings.Secure.getString (context.getContentResolver(),
		                                                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		        if (null == str) {
		            str = "";
		        } else {                
		            String[] list = str.split (",");
		            str = "";
		            int j = 0;
		            for (int i = 0; i < list.length; i++) {
		                if (!list[i].equals (LocationManager.GPS_PROVIDER)) {
		                    if (j > 0) {
		                        str += ",";
		                    }
		                    str += list[i];
		                    j++;
		                }
		            }
		            beforeEnable = str;
		        }
		    }
		    try {
		        Settings.Secure.putString (context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, beforeEnable);
		    } catch(Exception e) {
		    	showToast(e.getMessage());
		    }
	    }
	}
	
	public static boolean isGpsEnabled() {
		return ((LocationManager) context.getSystemService( Context.LOCATION_SERVICE )).isProviderEnabled( LocationManager.GPS_PROVIDER );
	}
	
	public static void showToast(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try{
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                } catch(Exception e) {

                }
            }
        });
    }

}
