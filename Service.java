package msandbox.za.co.projectlocate;

import java.net.URISyntaxException;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;

/**
 * Created by vhusha on 8/8/2015.
 */
public class Service extends android.app.Service {

    public static final String SOCKET_URL = "http://45.55.162.42/";
    private Handler handler = null;
    private com.github.nkzawa.socketio.client.Socket socketIO;

    public Service() {
    }

    @Override
    public void onCreate() {
    	handler= new Handler();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	runSocket();
        return Service.START_STICKY;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void showToast(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try{
                    Toast.makeText(getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                } catch(Exception e) {

                }
            }
        });
    }
    
    private void runSocket() {
        try {
            final com.github.nkzawa.socketio.client.Socket socket = IO.socket("http://45.55.168.42:80");
            socket.on(com.github.nkzawa.socketio.client.Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            }).on("command", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                	try{
                		JSONObject json = new JSONObject(args[0].toString());
                		int command = json.getInt("command");
                		dispatchCommand(command, json.getJSONObject("data"));
                	} catch(Exception ex) {
                		showToast("H1: " + ex.getMessage());
                	}
                }

            }).on(com.github.nkzawa.socketio.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            });
        	CommandHandler.set(getApplicationContext(), handler, socket);
            socket.connect();
        } catch (URISyntaxException e) {
        	showToast("H2: " + e.getMessage());
        }
    }

    protected void dispatchCommand(int command, JSONObject data) {
    	CommandHandler.commandDetector(command, data);
//    	int afterCommand = Util.getIfNotNull("afterCommand", data, 0);
//    	if(afterCommand != 0) {
//    		// After command can only be 1 command. v1.0
//    		CommandHandler.commandDetector(afterCommand, null);
//    	}
	}

	public String getDeviceIMEI() {
        return ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE))
                .getDeviceId();
    }
	public static int getLineNumber(Exception ex) {
	    return ex.getStackTrace()[4].getLineNumber();
	}
	public static String getFileName(Exception ex) {
	    return ex.getStackTrace()[4].getFileName();
	}
}
