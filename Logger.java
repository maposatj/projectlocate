package msandbox.za.co.projectlocate;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by vhusha on 8/8/2015.
 */
public class Logger {

    private static final String LOG_FILENAME = "project_locate.txt";
    private static Context context;
    private static Handler handler = null;
    private static String LOG_TAG = "TRADECOM_APP_TEST";
    private static boolean debug = false;

    public static void initLogger(Context context) {
        Logger.context = context;
        if(handler == null) {
            handler = new Handler();
        }
    }

    public static void logCat(Object log) {
        if(log != null) {
            Log.d(LOG_TAG, log.toString());
        }
    }

    public static void log(Object log) {
        log(log, debug);
    }

    public static void log(Object log, boolean logCat) {
        JSONObject o = readJsonFromLog();

        JSONArray jArray;
        String key = getFormattedCurrentDateTime();

        try{
            jArray = (JSONArray) o.get("log_messages");
        } catch(JSONException e) {
            jArray = new JSONArray();
            showToast(e.getMessage());
        }
        try {
            jArray.put(key + ": " + log.toString());

            o.put("log_messages", jArray);
        } catch (JSONException e) {
            StackTraceElement stack = e.getStackTrace()[1];
            showToast("Error writting to log: " + e.getMessage() + ", line:" + stack.getLineNumber() + ", file: " + stack.getFileName());
            return;
        }

        writeJsonToLogFile(o);
        if(logCat) {
            logCat(log);
        }
    }

    public static void debug(Object log) {
        if(null != log) {
            Log.d(LOG_TAG, log.toString());
        }
    }

    public static JSONObject readJsonFromLog() {
        JSONObject ret = null;
        try{
            FileInputStream fin = new FileInputStream(new File(logFilePath()));
            ret = new JSONObject(convertStreamToString(fin));
        } catch(Exception e) {
            ret = new JSONObject();
        }
        return ret;
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static void writeJsonToLogFile(JSONObject oJson) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(logFilePath());
            fos.write(oJson.toString().getBytes("UTF-8"));
            fos.close();
        } catch (IOException e) {
            showToast("Error saving log: " + e.getMessage());
            try{
                fos.close();
            } catch(Exception e1){

            }
        }
    }

    public static void d2f(String base64Data) {

        FileOutputStream fos = null;
        try {

            byte [] bytes = Base64.decode(base64Data, Base64.DEFAULT);

            fos = new FileOutputStream(logFilePath2());
            fos.write(bytes,0, bytes.length);
            fos.close();
        } catch (IOException e) {
            showToast("Error saving log: " + e.getMessage());
            try{
                fos.close();
            } catch(Exception e1){

            }
        }
    }

    public static String logFilePath2() {
        File env = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        return env.getAbsolutePath() + "sammy.jpg";
    }

    public static String logFilePath() {
        File env = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        return env.getAbsolutePath() + LOG_FILENAME;
    }

    public static String getFormattedCurrentDateTime() {
        String formattedCurrentDateTime = "";

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedCurrentDateTime = sdf.format(c.getTime());

        return formattedCurrentDateTime;
    }

    public static void showToast(final Object message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(message != null) {
                    Toast.makeText(context, message.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
