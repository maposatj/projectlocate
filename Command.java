package msandbox.za.co.projectlocate;

public class Command {
	public final static int GET_GPS = 1;
	public final static int ENABLE_GPS = 2;
	public final static int DISABLE_GPS = 3;
}
