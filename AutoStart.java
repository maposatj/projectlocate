package msandbox.za.co.projectlocate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by vhusha on 8/8/2015.
 */
public class AutoStart extends BroadcastReceiver {

    public void onReceive(Context arg0, Intent arg1) {
        Intent intent = new Intent(arg0,Service.class);
        arg0.startService(intent);
    }
}
